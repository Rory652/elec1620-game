//
// Created by Rory on 07/04/2021.
//

// I will test a variety of different approaches to different problems here when I'm stuck

#include <iostream>
#include <deque>
#include <cstdlib>
#include <chrono>
#include "mbed.h"
#include "stm32f413h_discovery_lcd.h"

// These functions directly change how the game plays/displayed
const int groundDetailRate = 8;
const int cloudRate = 120;
const int obstacleRate = 240;
const int playerX = 40;
const int baseScroll = 2;

// Specifically for variations in the base gound height
typedef struct pixelData {
    int x;
    int y; // Relative Y
} pixelData;

typedef struct detailData {
    int x;
    int y; // Relative Y
    int length;
} detailData;

typedef struct cloudData {
    int x;
    int y;
} cloudData;

typedef struct obstacleData {
    int x;
    int y;
    int type;
    int height;
    int width;
} obstacleData;

typedef struct character {
    int y; // Don't need x as it never changes
    int state; // 0: normal, 1: jumping, 2: crouching, 3: dead
    int animationFrame; // Used to change between animation frames
} character;

// All Functions Declaration
// Background Functions
void background();
void drawGround();
void drawDetails();
void drawClouds();
void drawCloud(int x, int y);
void drawObstacles();
void updateGround(int x);
void updateDetails(int x);
void updateClouds(int x);
void updateObstacles(int x);
int getNewGround();
int randomTwoNumbers(int one, int two);
int randomFourNumbers(int one, int two, int three, int four);

void drawCollision(int x, int y, int width, int height);

// Character Functions
void characterMain();
void characterJump();
void drawCharacter(int state, int animation);

// Menu Functions
void menu();
void gameOver();
void drawMenuStart();
void drawMenu(int score);

// Game Functions
int game();
void drawScore(int score);
int checkCollision();

// Global Variable
int frame = 0;
int highScore = 0;
std::deque<pixelData> ground;
std::deque<detailData> details;
std::deque<cloudData> clouds;
std::deque<obstacleData> obstacles;

character player;
DigitalIn jumpButton(p5);
DigitalIn crouchButton(p6);

DigitalIn exitButton(p7); // VERY TEMPORARY - Until collision is implemented

// Sprites
const int cloud[][2] = {{1, 0}, {2, 0}, {3, 0}, {4, 0}, {5, 0}, {6, 0}, {7, 0}, {8, 0}, {9, 0}, {10, 0}, {11, 0}, {12, 0},
                        {13, 0}, {14, 0}, {15, 0}, {16, 0}, {17, 0}, {18, 0}, {19, 0}, {20, 2}, {20, 1}, {19, 3}, {18, 4},
                        {17, 4}, {16, 5}, {16, 6}, {15, 7}, {14, 7}, {13, 8}, {12, 8}, {11, 8}, {10, 8}, {9, 7}, {8, 7},
                        {7, 6}, {6, 5}, {5, 5}, {4, 5}, {4, 4}, {3, 3}, {2, 3}, {1, 3}, {0, 3}, {0, 2}, {0, 1}};
const int cloudWidth = 21;

const int walk_one[][2] = {{10, 0}, {11, 0}, {12, 0}, {13, 0}, {14, 0}, {15, 0}, {10, 1}, {11, 1}, {12, 1}, {13, 1}, {14, 1},
                           {2, 2}, {3, 2}, {4, 2}, {5, 2}, {6, 2}, {7, 2}, {8, 2}, {9, 2}, {16, 2}, {17, 2}, {2, 3}, {3, 3},
                           {4, 3}, {5, 3}, {6, 3}, {7, 3}, {8, 3}, {9, 3}, {16, 3}, {17, 3},{0, 4}, {1, 4}, {2, 4}, {3, 4},
                           {14, 4}, {15, 4}, {0, 5}, {1, 5}, {2, 5}, {3, 5}, {14, 5}, {15, 5},{2, 6}, {3, 6}, {4, 6}, {5, 6},
                           {16, 6}, {17, 6}, {2, 7}, {3, 7}, {4, 7}, {5, 7}, {16, 7}, {17, 7}, {0, 8}, {1, 8}, {2, 8}, {3, 8},
                           {4, 8}, {5, 8}, {6, 8}, {7, 8}, {8, 8}, {9, 8}, {16, 8}, {17, 8}, {0, 9}, {1, 9}, {2, 9}, {3, 9},
                           {4, 9}, {5, 9}, {6, 9}, {7, 9}, {8, 9}, {9, 9}, {16, 9}, {17, 9}, {0, 10}, {1, 10}, {2, 10},
                           {3, 10}, {4, 10}, {5, 10}, {6, 10}, {7, 10}, {8, 10}, {9, 10}, {10, 10}, {11, 10}, {16, 10},
                           {17, 10}, {0, 11}, {1, 11}, {2, 11}, {3, 11}, {4, 11}, {5, 11}, {6, 11}, {7, 11}, {8, 11}, {9, 11},
                           {10, 11}, {11, 11}, {16, 11}, {17, 11},{2, 12}, {3, 12}, {4, 12}, {5, 12}, {6, 12}, {7, 12},
                           {8, 12}, {9, 12}, {10, 12}, {11, 12}, {16, 12}, {17, 12}, {2, 13}, {3, 13}, {4, 13}, {5, 13},
                           {6, 13}, {7, 13}, {8, 13}, {9, 13}, {10, 13}, {11, 13}, {16, 13}, {17, 13},{4, 14}, {5, 14},
                           {6, 14}, {7, 14}, {8, 14}, {9, 14}, {10, 14}, {11, 14}, {16, 14}, {17, 14}, {4, 15}, {5, 15},
                           {6, 15}, {7, 15}, {8, 15}, {9, 15}, {10, 15}, {11, 15}, {16, 15}, {17, 15},{4, 16}, {5, 16},
                           {6, 16}, {7, 16}, {8, 16}, {9, 16}, {14, 16}, {15, 16}, {4, 17}, {5, 17}, {6, 17}, {7, 17},
                           {8, 17}, {9, 17}, {14, 17}, {16, 17},{6, 18}, {7, 18}, {8, 18}, {9, 18}, {10, 18}, {11, 18},
                           {12, 18}, {13, 18}, {14, 18}, {15, 18}, {6, 19}, {7, 19}, {8, 19}, {9, 19}, {10, 19}, {11, 19},
                           {12, 19}, {13, 19}, {14, 19}, {15, 19},{6, 20}, {7, 20}, {8, 20}, {9, 20}, {16, 20}, {17, 20},
                           {6, 21}, {7, 21}, {8, 21}, {9, 21}, {16, 21}, {17, 21},{6, 22}, {7, 22}, {12, 22}, {13, 22},
                           {16, 22}, {17, 22}, {18, 22}, {19, 22}, {6, 23}, {7, 23}, {12, 23}, {13, 23}, {16, 23}, {17, 23},
                           {18, 23}, {19, 23},{6, 24}, {7, 24}, {8, 24}, {9, 24}, {14, 24}, {15, 24}, {16, 24}, {17, 24},
                           {6, 25}, {7, 25}, {8, 25}, {9, 25}, {14, 25}, {15, 25}, {16, 25}, {17, 25},{8, 26}, {9, 26},
                           {10, 26}, {11, 26}, {12, 26}, {13, 26}, {14, 26}, {15, 26}, {8, 27}, {9, 27}, {10, 27}, {11, 27},
                           {12, 27}, {13, 27}, {14, 27}, {15, 27}, {15, 1},};

const int walk_two[][2] = {{4, 0}, {5, 0}, {6, 0}, {7, 0}, {8, 0}, {9, 0}, {4, 1}, {5, 1}, {6, 1}, {7, 1}, {8, 1}, {9, 1},
                           {2, 2}, {3, 2}, {10, 2}, {11, 2}, {12, 2}, {13, 2}, {14, 2}, {15, 2}, {16, 2}, {17, 2}, {2, 3},
                           {3, 3}, {10, 3}, {11, 3}, {12, 3}, {13, 3}, {14, 3}, {15, 3}, {16, 3}, {17, 3}, {0, 4}, {1, 4},
                           {2, 4}, {3, 4}, {14, 4}, {15, 4}, {0, 5}, {1, 5}, {2, 5}, {3, 5}, {14, 5}, {15, 5},{2, 6},
                           {3, 6}, {4, 6}, {5, 6}, {16, 6}, {17, 6}, {2, 7}, {3, 7}, {4, 7}, {5, 7}, {16, 7}, {17, 7},
                           {0, 8}, {1, 8}, {2, 8}, {3, 8}, {4, 8}, {5, 8}, {6, 8}, {7, 8}, {8, 8}, {9, 8}, {16, 8}, {17, 8},
                           {0, 9}, {1, 9}, {2, 9}, {3, 9}, {4, 9}, {5, 9}, {6, 9}, {7, 9}, {8, 9}, {9, 9}, {16, 9}, {17, 9},
                           {0, 10}, {1, 10}, {2, 10}, {3, 10}, {4, 10}, {5, 10}, {6, 10}, {7, 10}, {8, 10}, {9, 10}, {10, 10},
                           {11, 10}, {16, 10}, {17, 10}, {0, 11}, {1, 11}, {2, 11}, {3, 11}, {4, 11}, {5, 11}, {6, 11},
                           {7, 11}, {8, 11}, {9, 11}, {10, 11}, {11, 11}, {16, 11}, {17, 11},{2, 12}, {3, 12}, {4, 12},
                           {5, 12}, {6, 12}, {7, 12}, {8, 12}, {9, 12}, {10, 12}, {11, 12}, {16, 12}, {17, 12}, {2, 13},
                           {3, 13}, {4, 13}, {5, 13}, {6, 13}, {7, 13}, {8, 13}, {9, 13}, {10, 13}, {11, 13}, {16, 13},
                           {17, 13},{4, 14}, {5, 14}, {6, 14}, {7, 14}, {8, 14}, {9, 14}, {10, 14}, {11, 14}, {16, 14},
                           {17, 14}, {4, 15}, {5, 15}, {6, 15}, {7, 15}, {8, 15}, {9, 15}, {10, 15}, {11, 15}, {16, 15},
                           {17, 15},{4, 16}, {5, 16}, {6, 16}, {7, 16}, {8, 16}, {9, 16}, {14, 16}, {15, 16}, {4, 17},
                           {5, 17}, {6, 17}, {7, 17}, {8, 17}, {9, 17}, {14, 17}, {16, 17},{6, 18}, {7, 18}, {8, 18},
                           {9, 18}, {10, 18}, {11, 18}, {12, 18}, {13, 18}, {14, 18}, {15, 18}, {6, 19}, {7, 19}, {8, 19},
                           {9, 19}, {10, 19}, {11, 19}, {12, 19}, {13, 19}, {14, 19}, {15, 19},{6, 20}, {7, 20}, {8, 20},
                           {9, 20}, {16, 20}, {17, 20}, {6, 21}, {7, 21}, {8, 21}, {9, 21}, {16, 21}, {17, 21},{6, 22},
                           {7, 22}, {12, 22}, {13, 22}, {16, 22}, {17, 22}, {18, 22}, {19, 22}, {6, 23}, {7, 23}, {12, 23},
                           {13, 23}, {16, 23}, {17, 23}, {18, 23}, {19, 23},{6, 24}, {7, 24}, {8, 24}, {9, 24}, {14, 24},
                           {15, 24}, {16, 24}, {17, 24}, {6, 25}, {7, 25}, {8, 25}, {9, 25}, {14, 25}, {15, 25}, {16, 25},
                           {17, 25},{8, 26}, {9, 26}, {10, 26}, {11, 26}, {12, 26}, {13, 26}, {14, 26}, {15, 26}, {8, 27},
                           {9, 27}, {10, 27}, {11, 27}, {12, 27}, {13, 27}, {14, 27}, {15, 27}};

const int jump[][2] = {{2, 2}, {3, 2}, {4, 2}, {5, 2}, {6, 2}, {7, 2}, {8, 2}, {9, 2}, {10, 2}, {11, 2}, {12, 2}, {13, 2},
                       {14, 2}, {15, 2}, {16, 2}, {17, 2}, {2, 3}, {3, 3}, {4, 3}, {5, 3}, {6, 3}, {7, 3}, {8, 3}, {9, 3},
                       {10, 3}, {11, 3}, {12, 3}, {13, 3}, {14, 3}, {15, 3}, {16, 3}, {17, 3},{0, 4}, {1, 4}, {2, 4}, {3, 4},
                       {14, 4}, {15, 4}, {0, 5}, {1, 5}, {2, 5}, {3, 5}, {14, 5}, {15, 5},{2, 6}, {3, 6}, {4, 6}, {5, 6},
                       {16, 6}, {17, 6}, {2, 7}, {3, 7}, {4, 7}, {5, 7}, {16, 7}, {17, 7},{0, 8}, {1, 8}, {2, 8}, {3, 8},
                       {4, 8}, {5, 8}, {6, 8}, {7, 8}, {8, 8}, {9, 8}, {16, 8}, {17, 8}, {0, 9}, {1, 9}, {2, 9}, {3, 9},
                       {4, 9}, {5, 9}, {6, 9}, {7, 9}, {8, 9}, {9, 9}, {16, 9}, {17, 9},{0, 10}, {1, 10}, {2, 10}, {3, 10},
                       {4, 10}, {5, 10}, {6, 10}, {7, 10}, {8, 10}, {9, 10}, {10, 10}, {11, 10}, {16, 10}, {17, 10}, {0, 11},
                       {1, 11}, {2, 11}, {3, 11}, {4, 11}, {5, 11}, {6, 11}, {7, 11}, {8, 11}, {9, 11}, {10, 11}, {11, 11},
                       {16, 11}, {17, 11},{2, 12}, {3, 12}, {4, 12}, {5, 12}, {6, 12}, {7, 12}, {8, 12}, {9, 12}, {10, 12},
                       {11, 12}, {16, 12}, {17, 12}, {2, 13}, {3, 13}, {4, 13}, {5, 13}, {6, 13}, {7, 13}, {8, 13}, {9, 13},
                       {10, 13}, {11, 13}, {16, 13}, {17, 13},{4, 14}, {5, 14}, {6, 14}, {7, 14}, {8, 14}, {9, 14}, {10, 14},
                       {11, 14}, {16, 14}, {17, 14}, {4, 15}, {5, 15}, {6, 15}, {7, 15}, {8, 15}, {9, 15}, {10, 15}, {11, 15},
                       {16, 15}, {17, 15},{4, 16}, {5, 16}, {6, 16}, {7, 16}, {8, 16}, {9, 16}, {14, 16}, {15, 16}, {4, 17},
                       {5, 17}, {6, 17}, {7, 17}, {8, 17}, {9, 17}, {14, 17}, {16, 17},{6, 18}, {7, 18}, {8, 18}, {9, 18},
                       {10, 18}, {11, 18}, {12, 18}, {13, 18}, {14, 18}, {15, 18}, {6, 19}, {7, 19}, {8, 19}, {9, 19}, {10, 19},
                       {11, 19}, {12, 19}, {13, 19}, {14, 19}, {15, 19},{6, 20}, {7, 20}, {8, 20}, {9, 20}, {16, 20}, {17, 20},
                       {6, 21}, {7, 21}, {8, 21}, {9, 21}, {16, 21}, {17, 21},{6, 22}, {7, 22}, {12, 22}, {13, 22}, {16, 22},
                       {17, 22}, {18, 22}, {19, 22}, {6, 23}, {7, 23}, {12, 23}, {13, 23}, {16, 23}, {17, 23}, {18, 23}, {19, 23},
                       {6, 24}, {7, 24}, {8, 24}, {9, 24}, {14, 24}, {15, 24}, {16, 24}, {17, 24}, {6, 25}, {7, 25}, {8, 25},
                       {9, 25}, {14, 25}, {15, 25}, {16, 25}, {17, 25},{8, 26}, {9, 26}, {10, 26}, {11, 26}, {12, 26}, {13, 26},
                       {14, 26}, {15, 26}, {8, 27}, {9, 27}, {10, 27}, {11, 27}, {12, 27}, {13, 27}, {14, 27}, {15, 27}};

const int crouch[][2] = {{0, 0}, {1, 0}, {4, 0}, {5, 0}, {6, 0}, {7, 0}, {8, 0}, {9, 0}, {10, 0}, {11, 0}, {12, 0}, {13, 0},
                         {0, 1}, {1, 1}, {4, 1}, {5, 1}, {6, 1}, {7, 1}, {8, 1}, {9, 1}, {10, 1}, {11, 1}, {12, 1}, {13, 1},
                         {0, 2}, {1, 2}, {2, 2}, {3, 2}, {14, 2}, {15, 2}, {16, 2}, {17, 2}, {18, 2}, {19, 2}, {20, 2}, {21, 2},
                         {22, 2}, {23, 2}, {24, 2}, {25, 2}, {0, 3}, {1, 3}, {2, 3}, {3, 3}, {14, 3}, {15, 3}, {16, 3}, {17, 3},
                         {18, 3}, {19, 3}, {20, 3}, {21, 3}, {22, 3}, {23, 3}, {24, 3}, {25, 3},{0, 4}, {1, 4}, {16, 4}, {17, 4},
                         {18, 4}, {19, 4}, {26, 4}, {27, 4}, {0, 5}, {1, 5}, {16, 5}, {17, 5}, {18, 5}, {19, 5}, {26, 5}, {27, 5},
                         {0, 6}, {1, 6}, {8, 6}, {9, 6}, {10, 6}, {11, 6}, {12, 6}, {13, 6}, {16, 6}, {17, 6}, {22, 6}, {23, 6},
                         {26, 6}, {27, 6}, {28, 6}, {29, 6}, {0, 7}, {1, 7}, {8, 7}, {9, 7}, {10, 7}, {11, 7}, {12, 7}, {13, 7},
                         {16, 7}, {17, 7}, {22, 7}, {23, 7}, {26, 7}, {27, 7}, {28, 7}, {29, 7},{0, 8}, {1, 8}, {6, 8}, {7, 8},
                         {8, 8}, {9, 8}, {10, 8}, {11, 8}, {12, 8}, {13, 8}, {14, 8}, {15, 8}, {16, 8}, {17, 8}, {18, 8}, {19, 8},
                         {24, 8}, {25, 8}, {26, 8}, {27, 8}, {0, 9}, {1, 9}, {6, 9}, {7, 9}, {8, 9}, {9, 9}, {10, 9}, {11, 9},
                         {12, 9}, {13, 9}, {14, 9}, {15, 9}, {16, 9}, {17, 9}, {18, 9}, {19, 9}, {24, 9}, {25, 9}, {26, 9},
                         {27, 9},{0, 10}, {1, 10}, {6, 10}, {7, 10}, {8, 10}, {9, 10}, {10, 10}, {11, 10}, {12, 10}, {13, 10},
                         {14, 10}, {15, 10}, {18, 10}, {19, 10}, {20, 10}, {21, 10}, {22, 10}, {23, 10}, {24, 10}, {25, 10},
                         {0, 11}, {1, 11}, {6, 11}, {7, 11}, {8, 11}, {9, 11}, {10, 11}, {11, 11}, {12, 11}, {13, 11}, {14, 11},
                         {15, 11}, {18, 11}, {19, 11}, {20, 11}, {21, 11}, {22, 11}, {23, 11}, {24, 11}, {25, 11},{0, 12},
                         {1, 12}, {4, 12}, {5, 12}, {6, 12}, {7, 12}, {8, 12}, {9, 12}, {10, 12}, {11, 12}, {12, 12}, {13, 12},
                         {14, 12}, {15, 12}, {0, 13}, {1, 13}, {4, 13}, {5, 13}, {6, 13}, {7, 13}, {8, 13}, {9, 13}, {10, 13},
                         {11, 13}, {12, 13}, {13, 13}, {14, 13}, {15, 13},{0, 14}, {1, 14}, {2, 14}, {3, 14}, {4, 14}, {5, 14},
                         {6, 14}, {7, 14}, {8, 14}, {9, 14}, {10, 14}, {11, 14}, {0, 15}, {1, 15}, {2, 15}, {3, 15}, {4, 15},
                         {5, 15}, {6, 15}, {7, 15}, {8, 15}, {9, 15}, {10, 15}, {11, 15},{2, 16}, {3, 16}, {6, 16}, {7, 16},
                         {8, 16}, {9, 16}, {2, 17}, {3, 17}, {6, 17}, {7, 17}, {8, 17}, {9, 17}};

const int characterDimensions[][2] = {{19, 27}, {29, 17}, {19, 27}};

const int obstacle_one[][2] = {{9, 0}, {10, 0}, {11, 0}, {12, 0}, {13, 0}, {14, 0}, {15, 0}, {16, 0}, {17, 0}, {18, 0},
                               {19, 0}, {11, 1}, {12, 1}, {13, 1}, {14, 1}, {15, 1}, {16, 1}, {17, 1}, {11, 2}, {12, 2},
                               {13, 2}, {14, 2}, {15, 2}, {16, 2}, {17, 2}, {12, 3}, {13, 3}, {14, 3}, {15, 3}, {16, 3},
                               {12, 4}, {13, 4}, {14, 4}, {15, 4}, {16, 4}, {13, 5}, {14, 5}, {15, 5}, {13, 6}, {14, 6},
                               {15, 6}, {13, 7}, {14, 7}, {15, 7}, {13, 8}, {14, 8}, {15, 8}, {13, 9}, {14, 9}, {15, 9},
                               {13, 10}, {14, 10}, {15, 10}, {13, 11}, {14, 11}, {15, 11}, {13, 12}, {14, 12}, {15, 12},
                               {13, 13}, {14, 13}, {15, 13}, {13, 14}, {14, 14}, {15, 14}, {13, 15}, {14, 15}, {15, 15},
                               {13, 16}, {14, 16}, {15, 16}, {13, 17}, {14, 17}, {15, 17}, {13, 18}, {14, 18}, {15, 18},
                               {13, 19}, {14, 19}, {15, 19}, {13, 20}, {14, 20}, {15, 20}, {13, 21}, {14, 21}, {15, 21},
                               {13, 22}, {14, 22}, {15, 22}, {13, 23}, {14, 23}, {15, 23}, {13, 24}, {14, 24}, {15, 24},
                               {13, 25}, {14, 25}, {15, 25}, {13, 26}, {14, 26}, {15, 26}, {12, 27}, {13, 27}, {14, 27},
                               {15, 27}, {16, 27}, {11, 28}, {12, 28}, {13, 28}, {14, 28}, {15, 28}, {16, 28}, {17, 28},
                               {1, 29}, {2, 29}, {3, 29}, {4, 29}, {5, 29}, {6, 29}, {7, 29}, {8, 29}, {9, 29}, {10, 29},
                               {11, 29}, {12, 29}, {13, 29}, {14, 29}, {15, 29}, {16, 29}, {17, 29}, {18, 29}, {19, 29},
                               {20, 29}, {21, 29}, {22, 29}, {23, 29}, {24, 29}, {25, 29}, {0, 30}, {1, 30}, {2, 30},
                               {3, 30}, {4, 30}, {5, 30}, {6, 30}, {7, 30}, {8, 30}, {9, 30}, {10, 30}, {11, 30}, {12, 30},
                               {13, 30}, {14, 30}, {15, 30}, {16, 30}, {17, 30}, {18, 30}, {19, 30}, {20, 30}, {21, 30},
                               {22, 30}, {23, 30}, {24, 30}, {25, 30}, {26, 30}, {0, 31}, {26, 31}, {27, 31}, {0, 32},
                               {2, 32}, {5, 32}, {7, 32}, {8, 32}, {9, 32}, {10, 32}, {12, 32}, {18, 32}, {19, 32}, {22, 32},
                               {25, 32}, {27, 32}, {28, 32}, {0, 33}, {2, 33}, {4, 33}, {5, 33}, {7, 33}, {10, 33}, {12, 33},
                               {18, 33}, {19, 33}, {22, 33}, {23, 33}, {24, 33}, {25, 33}, {28, 33}, {29, 33}, {0, 34},
                               {2, 34}, {3, 34}, {5, 34}, {7, 34}, {10, 34}, {12, 34}, {18, 34}, {19, 34}, {22, 34}, {25, 34},
                               {28, 34}, {29, 34}, {0, 35}, {2, 35}, {5, 35}, {7, 35}, {8, 35}, {9, 35}, {10, 35}, {12, 35},
                               {13, 35}, {14, 35}, {15, 35}, {17, 35}, {18, 35}, {19, 35}, {20, 35}, {22, 35}, {25, 35},
                               {27, 35}, {28, 35}, {0, 36}, {26, 36}, {27, 36}, {0, 37}, {1, 37}, {2, 37}, {3, 37}, {4, 37},
                               {5, 37}, {6, 37}, {7, 37}, {8, 37}, {9, 37}, {10, 37}, {11, 37}, {12, 37}, {13, 37}, {14, 37},
                               {15, 37}, {16, 37}, {17, 37}, {18, 37}, {19, 37}, {20, 37}, {21, 37}, {22, 37}, {23, 37},
                               {24, 37}, {25, 37}, {26, 37}, {1, 38}, {2, 38}, {3, 38}, {4, 38}, {5, 38}, {6, 38}, {7, 38},
                               {8, 38}, {9, 38}, {10, 38}, {11, 38}, {12, 38}, {13, 38}, {14, 38}, {15, 38}, {16, 38},
                               {17, 38}, {18, 38}, {19, 38}, {20, 38}, {21, 38}, {22, 38}, {23, 38}, {24, 38}, {25, 38}};

const int obstacle_two[][2] = {{16,  0}, {17, 0}, {18, 0}, {19, 0}, {20, 0}, {21, 0}, {22, 0}, {23, 0}, {27, 0}, {28, 0},
                               {29, 0}, {30, 0}, {31, 0}, {32, 0}, {33, 0}, {34, 0}, {35, 0}, {36, 0}, {37, 0}, {42, 0},
                               {43, 0}, {44, 0}, {45, 0}, {15, 1}, {16, 1}, {17, 1}, {18, 1}, {19, 1}, {20, 1}, {21, 1},
                               {22, 1}, {23, 1}, {24, 1}, {27, 1}, {28, 1}, {29, 1}, {30, 1}, {31, 1}, {32, 1}, {33, 1},
                               {34, 1}, {35, 1}, {36, 1}, {37, 1}, {38, 1}, {41, 1}, {42, 1}, {43, 1}, {44, 1}, {45, 1},
                               {46, 1}, {8, 2}, {9, 2}, {10, 2}, {15, 2}, {16, 2}, {18, 2}, {20, 2}, {24, 2}, {25, 2},
                               {28, 2}, {30, 2}, {32, 2}, {38, 2}, {39, 2}, {41, 2}, {43, 2}, {46, 2}, {47, 2}, {7, 3},
                               {8, 3}, {9, 3}, {10, 3}, {11, 3}, {16, 3}, {17, 3}, {25, 3}, {26, 3}, {29, 3}, {30, 3},
                               {39, 3}, {42, 3}, {47, 3}, {48, 3}, {6, 4}, {7, 4}, {11, 4}, {12, 4}, {17, 4}, {18, 4},
                               {25, 4}, {26, 4}, {30, 4}, {31, 4}, {39, 4}, {40, 4}, {41, 4}, {48, 4}, {49, 4}, {6, 5},
                               {7, 5}, {10, 5}, {11, 5}, {12, 5}, {13, 5}, {16, 5}, {17, 5}, {25, 5}, {26, 5}, {30, 5},
                               {31, 5}, {39, 5}, {40, 5}, {48, 5}, {49, 5}, {6, 6}, {7, 6}, {11, 6}, {12, 6}, {13, 6},
                               {14, 6}, {15, 6}, {16, 6}, {17, 6}, {24, 6}, {25, 6}, {30, 6}, {31, 6}, {39, 6}, {40, 6},
                               {48, 6}, {49, 6}, {6, 7}, {7, 7}, {11, 7}, {14, 7}, {15, 7}, {16, 7}, {17, 7}, {24, 7},
                               {25, 7}, {30, 7}, {31, 7}, {40, 7}, {41, 7}, {47, 7}, {48, 7}, {6, 8}, {7, 8}, {16, 8},
                               {17, 8}, {24, 8}, {25, 8}, {26, 8}, {27, 8}, {28, 8}, {29, 8}, {30, 8}, {31, 8}, {32, 8},
                               {41, 8}, {42, 8}, {46, 8}, {47, 8}, {7, 9}, {8, 9}, {15, 9}, {16, 9}, {24, 9}, {25, 9},
                               {26, 9}, {27, 9}, {28, 9}, {29, 9}, {30, 9}, {31, 9}, {32, 9}, {42, 9}, {43, 9}, {45, 9},
                               {46, 9}, {8, 10}, {9, 10}, {15, 10}, {16, 10}, {23, 10}, {24, 10}, {32, 10}, {33, 10},
                               {43, 10}, {44, 10}, {45, 10}, {46, 10}, {9, 11}, {10, 11}, {14, 11}, {15, 11}, {22, 11},
                               {23, 11}, {33, 11}, {34, 11}, {44, 11}, {45, 11}, {10, 12}, {11, 12}, {14, 12}, {15, 12},
                               {22, 12}, {23, 12}, {33, 12}, {34, 12}, {45, 12}, {46, 12}, {11, 13}, {12, 13}, {13, 13},
                               {14, 13}, {22, 13}, {34, 13}, {35, 13}, {45, 13}, {46, 13}, {12, 14}, {13, 14}, {35, 14},
                               {45, 14}, {46, 14}, {11, 15}, {12, 15}, {45, 15}, {46, 15}, {11, 16}, {12, 16}, {45, 16},
                               {46, 16}, {10, 17}, {11, 17}, {45, 17}, {46, 17}, {8, 18}, {9, 18}, {10, 18}, {45, 18},
                               {46, 18}, {6, 19}, {7, 19}, {8, 19}, {9, 19}, {45, 19}, {46, 19}, {3, 20}, {4, 20}, {5, 20},
                               {6, 20}, {7, 20}, {45, 20}, {46, 20}, {2, 21}, {3, 21}, {4, 21}, {5, 21}, {45, 21}, {46, 21},
                               {1, 22}, {2, 22}, {44, 22}, {45, 22}, {0, 23}, {1, 23}, {3, 23}, {4, 23}, {5, 23}, {6, 23},
                               {44, 23}, {45, 23}, {0, 24}, {44, 24}, {45, 24}, {0, 25}, {43, 25}, {44, 25}, {0, 26},
                               {2, 26}, {43, 26}, {44, 26}, {0, 27}, {1, 27}, {2, 27}, {3, 27}, {8, 27}, {42, 27}, {43, 27},
                               {1, 28}, {2, 28}, {3, 28}, {4, 28}, {5, 28}, {8, 28}, {15, 28}, {21, 28}, {22, 28}, {23, 28},
                               {24, 28}, {41, 28}, {42, 28}, {3, 29}, {4, 29}, {5, 29}, {6, 29}, {13, 29}, {16, 29}, {17, 29},
                               {18, 29}, {19, 29}, {20, 29}, {21, 29}, {22, 29}, {23, 29}, {24, 29}, {25, 29}, {26, 29},
                               {27, 29}, {28, 29}, {29, 29}, {30, 29}, {31, 29}, {32, 29}, {33, 29}, {34, 29}, {35, 29},
                               {36, 29}, {37, 29}, {38, 29}, {39, 29}, {40, 29}, {41, 29}, {6, 30}, {7, 30}, {8, 30}, {9, 30},
                               {10, 30}, {11, 30}, {12, 30}, {13, 30}, {16, 30}, {17, 30}, {18, 30}, {19, 30}, {20, 30},
                               {25, 30}, {26, 30}, {27, 30}, {28, 30}, {29, 30}, {30, 30}, {31, 30}, {32, 30}, {33, 30},
                               {34, 30}, {35, 30}, {36, 30}, {37, 30}, {38, 30}, {39, 30}, {40, 30}, {7, 31}, {8, 31},
                               {9, 31}, {10, 31}, {11, 31}, {12, 31}, {13, 31}, {14, 31}, {15, 31}, {16, 31}, {10, 32},
                               {11, 32}, {14, 32}, {15, 32}};

const int obstacle_three[][2] = {{1, 0}, {2, 0}, {3, 0}, {4, 0}, {5, 0}, {6, 0}, {7, 0}, {8, 0}, {9, 0}, {10, 0}, {11, 0},
                                 {12, 0}, {13, 0}, {14, 0}, {0, 1}, {1, 1}, {2, 1}, {3, 1}, {4, 1}, {5, 1}, {6, 1}, {7, 1},
                                 {8, 1}, {9, 1}, {10, 1}, {11, 1}, {12, 1}, {13, 1}, {14, 1}, {15, 1}, {16, 1}, {0, 2}, {2, 2},
                                 {3, 2}, {8, 2}, {9, 2}, {10, 2}, {16, 2}, {17, 2}, {0, 3}, {4, 3}, {7, 3}, {11, 3}, {17, 3},
                                 {18, 3}, {0, 4}, {1, 4}, {4, 4}, {7, 4}, {10, 4}, {18, 4}, {0, 5}, {1, 5}, {15, 5}, {17, 5},
                                 {18, 5}, {1, 6}, {2, 6}, {3, 6}, {6, 6}, {14, 6}, {15, 6}, {16, 6}, {17, 6}, {1, 7}, {2, 7},
                                 {3, 7}, {6, 7}, {14, 7}, {15, 7}, {1, 8}, {2, 8}, {3, 8}, {6, 8}, {13, 8}, {14, 8}, {1, 9},
                                 {2, 9}, {13, 9}, {14, 9}, {1, 10}, {2, 10}, {3, 10}, {5, 10}, {12, 10}, {13, 10}, {2, 11},
                                 {12, 11}, {13, 11}, {2, 12}, {3, 12}, {5, 12}, {8, 12}, {11, 12}, {12, 12}, {3, 13}, {4, 13},
                                 {10, 13}, {11, 13}, {4, 14}, {5, 14}, {6, 14}, {7, 14}, {8, 14}, {9, 14}, {10, 14}, {5, 15},
                                 {6, 15}, {7, 15}, {8, 15}, {9, 15}};

// Dimensions of the obstacles (to detect collision) - {x, y}
const int dimensions[][2] = {{29, 38} , {49, 32}, {18, 15}};

// Background Functions
void background() {
    if (frame == 0) {
        for (int i = 0; i < 240; i++) {
            updateGround(i);

            if (i % groundDetailRate == 0) {
                updateDetails(i);
            }

            if (i % cloudRate == 0) {
                updateClouds(i);
            }
        }
    }

    drawGround();
    drawDetails();
    drawClouds();
    drawObstacles();

    for (int i = 0; i < baseScroll; i++) {
        updateGround((241 - baseScroll) + i);
    }

    if ((frame * baseScroll) % groundDetailRate == 0) {
        updateDetails(240);
    }

    if ((frame * baseScroll) % cloudRate == 0) {
        updateClouds(240);
    }

    if ((frame * baseScroll) % obstacleRate == 0) {
        updateObstacles(240);
    }
}

void drawGround() {
    std::deque<pixelData> :: iterator element;
    int yPos = 200;
    BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
    BSP_LCD_FillRect(0, yPos, 240, 2); // Draws the base ground
    for (element = ground.begin(); element != ground.end(); element++) {
        BSP_LCD_DrawRect((*element).x, yPos + (*element).y, 1, 2);

        (*element).x -= baseScroll;
        if ((*element).x < 0) {
            ground.pop_front(); // X will increase as you go up the queue so the first will always have the lowest x
        }
    }
}

void drawDetails() {
    std::deque<detailData> :: iterator element;
    int yPos = 200;
    BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
    for (element = details.begin(); element != details.end(); element++) {
        BSP_LCD_DrawHLine((*element).x, yPos + (*element).y, (*element).length);

        (*element).x -= baseScroll;
        if ((*element).x + (*element).length <= 0) {
            details.pop_front(); // X will increase as you go up the queue so the first will always have the lowest x
        }
    }
}

void drawClouds() {
    std::deque<cloudData> :: iterator element;
    int yPos = 40;
    for (element = clouds.begin(); element != clouds.end(); element++) {
        drawCloud((*element).x, yPos + (*element).y);

        // Updates positions every 3 frames
        if (frame % 3 == 0) {
            (*element).x -= baseScroll;
            if ((*element).x + cloudWidth <= 0) {
                clouds.pop_front(); // X will increase as you go up the queue so the first will always have the lowest x
            }
        }
    }
}


void drawCloud(int x, int y) {
    for (auto i : cloud) {
        BSP_LCD_DrawPixel(x + i[0], y - i[1], LCD_COLOR_LIGHTGRAY);
    }
}

void drawObstacles() {
    std::deque<obstacleData> :: iterator element;
    for (element = obstacles.begin(); element != obstacles.end(); element++) {
        switch ((*element).type) {
            case 0:     // High and Wide
                for (auto i : obstacle_one) {
                    BSP_LCD_DrawPixel((*element).x + i[0], (*element).y - i[1], LCD_COLOR_GRAY);
                }
                break;
            case 1:     // High and Narrow
                for (auto i : obstacle_two) {
                    BSP_LCD_DrawPixel((*element).x + i[0], (*element).y - i[1], LCD_COLOR_GRAY);
                }
                break;
            default:     // Low and Wide
                for (auto i : obstacle_three) {
                    BSP_LCD_DrawPixel((*element).x + i[0], (*element).y - i[1], LCD_COLOR_GRAY);
                }
                break;
        }

        drawCollision((*element).x, (*element).y, (*element).width, (*element).height);

        (*element).x -= baseScroll;
        if ((*element).x + (*element).width < 0) {
            obstacles.pop_front(); // X will increase as you go up the queue so the first will always have the lowest x
        }
    }
}

void drawCollision(int x, int y, int width, int height) {
    BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
    BSP_LCD_DrawRect(x, (y - height), width, height);
}

void updateGround(int x) {
    pixelData newPixel;

    newPixel.x = x; // Always added onto the end
    newPixel.y = getNewGround();
    if (newPixel.y != 0) {
        ground.push_back(newPixel);
    }
}

void updateDetails(int x) {
    detailData newDetail;

    newDetail.x = x;
    newDetail.y = randomFourNumbers(0, 6, 10, 15);
    newDetail.length = randomTwoNumbers(5, 10);

    if (newDetail.y != 0) {
        details.push_back(newDetail);
    }
}

void updateClouds(int x) {
    cloudData newCloud;

    int addCloud = randomTwoNumbers(0, 1);

    if (addCloud == 1) {
        newCloud.x = x;
        newCloud.y = rand() % 60; // Height is random

        clouds.push_back(newCloud);
    }
}

void updateObstacles(int x) {
    int type = (rand() % 4) - 1;

    if (type >= 0) {
        obstacleData newObstacle;

        newObstacle.x = x;
        newObstacle.y = 200;
        newObstacle.type = type;
        newObstacle.width = dimensions[type][0];
        newObstacle.height = dimensions[type][1];

        obstacles.push_back(newObstacle);
    }
}

int getNewGround() {
    int random = rand() % 1000;

    if (random > 950) {
        return 1;     // Higher Elevation
    } else if (random < 50) {
        return -1;    // Lower Elevation
    } else {
        return 0;     // Normal Elevation (most frequent)
    }
}

// Randomly picks between 2 numbers with equal odds
int randomTwoNumbers(int one, int two) {
    int random = (rand() % 2) + 1;

    switch (random) {
        case 1:
            return one;
        default:
            return two;
    }
}

// Randomly picks between 4 numbers with equal odds
int randomFourNumbers(int one, int two, int three, int four) {
    int random = (rand() % 4) + 1;

    switch (random) {
        case 1:
            return one;
        case 2:
            return two;
        case 3:
            return three;
        default:
            return four;
    }
}

void characterMain() {
    if (frame == 0) {
        player.state = 0;
        player.animationFrame = 0;
        player.y = 199;
    }

    if (jumpButton) {
        player.state = 2;
    } else if (crouchButton && player.state != 2) {
        player.state = 1;
    } else if (player.y == 199) {
        // Dpn't want the animation to change while the character is in the air
        player.state = 0;
    }

    if (player.state == 2) {
        characterJump();
    }

    drawCharacter(player.state, player.animationFrame);

    if (frame % 2 == 0) {
        player.animationFrame = !player.animationFrame;
    }
}

void characterJump() {
    static int maxHeight = 25;
    static int up = 1;

    if (jumpButton && maxHeight < 75) {
        maxHeight += 2;
    }

    if (up) {
        if (player.y > 199 - maxHeight) {
            if (player.y < 199 - (maxHeight * 0.9)) {
                player.y--;
            } else {
                player.y -= 2;
            }
        } else {
            up = 0;
        }
    } else if (!up) {
        if (player.y < 199) {
            if (player.y < 199 - (maxHeight * 0.9)) {
                player.y++;
            } else {
                player.y += 2;
            }
        } else {
            player.y = 199;
            up = 1;
            maxHeight = 25;
        }
    }
}

void drawCharacter(int state, int animation) {
    // TODO: Tidy this up
    switch (state) {
        case 1:
            for (auto i : crouch) {
                BSP_LCD_DrawPixel(playerX + i[0], player.y - i[1], LCD_COLOR_BLACK);
            }
            break;
        case 2:
            for (auto i : jump) {
                BSP_LCD_DrawPixel(playerX + i[0], player.y - i[1], LCD_COLOR_BLACK);
            }
            break;
        default:
            if (animation == 0) {
                for (auto i : walk_one) {
                    BSP_LCD_DrawPixel(playerX + i[0], player.y - i[1], LCD_COLOR_BLACK);
                }
            } else {
                for (auto i : walk_two) {
                    BSP_LCD_DrawPixel(playerX + i[0], player.y - i[1], LCD_COLOR_BLACK);
                }
            }
    }

    drawCollision(playerX, player.y, characterDimensions[player.state][0], characterDimensions[player.state][1]);
}

void menu() {
    int score = 0;
    while (1) {
        // Display starting message
        if (score == 0) {
            drawMenuStart();
            score = 1;
        }

        if (jumpButton) {
            score = game();
            drawMenu(score);

            if (score > highScore) {
                highScore = score;
            }

            gameOver();
        }
        wait_us(10000);
    }
}

// Empties all of the queues ready for the next game
void gameOver() {
    frame = 0;

    ground.clear();
    details.clear();
    clouds.clear();
    obstacles.clear();
}

void drawMenuStart() {
    auto *display = (uint8_t *) "Press Jump to Start";

    BSP_LCD_Clear(LCD_COLOR_WHITE);
    BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
    BSP_LCD_SetFont(&Font16);
    BSP_LCD_DisplayStringAt(5, 104, display, CENTER_MODE);
}

void drawMenu(int score) {
    char displayScore[20];
    sprintf(displayScore, "Final Score: %5i", score);
    auto *displayRestart = (uint8_t *) "Press Jump to Restart";

    BSP_LCD_Clear(LCD_COLOR_WHITE);
    BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
    BSP_LCD_SetFont(&Font16);
    BSP_LCD_DisplayStringAt(5, 60, (uint8_t *) displayScore, CENTER_MODE);
    BSP_LCD_DisplayStringAt(5, 156, displayRestart, CENTER_MODE);

    if (score > highScore) {
        auto *displayHigh = (uint8_t *) "New High Score!";
        BSP_LCD_DisplayStringAt(5, 108, displayHigh, CENTER_MODE);
    } else {
        char displayHigh[20];
        sprintf(displayHigh, "High Score: %5i", highScore);
        BSP_LCD_DisplayStringAt(5, 108, (uint8_t *) displayHigh, CENTER_MODE);
    }
}

int game() {
    int score;

    while(1) {
        score = frame / 5;

        BSP_LCD_Clear(LCD_COLOR_WHITE);
        background();
        drawScore(score);
        characterMain();
        frame++;

        if (checkCollision()) {
            return score;
        }
        wait_us(100); // Small wait is needed otherwise the simulator crashes
    }
}

void drawScore(int score) {
    char displayScore[20];
    sprintf(displayScore, "SCORE: %4i", score);

    char displayHigh[20];
    sprintf(displayHigh, "HIGH SCORE: %4i", highScore);

    BSP_LCD_SetTextColor(LCD_COLOR_BLACK);
    BSP_LCD_SetFont(&Font12);
    BSP_LCD_DisplayStringAt(5, 5, (uint8_t *) displayScore, RIGHT_MODE);
    BSP_LCD_DisplayStringAt(5, 5, (uint8_t *) displayHigh, LEFT_MODE);
}

int checkCollision() {
    // Left X = obstacle.x, Right X = obstacle.x + obstacle.width
    // Low Y = obstacle.y, High Y = obstacle.y - obstacle.height

    // Get the first obstacle (always the closest to the player)
    obstacleData obstacle = obstacles[0];

    int playerD[2][2] = {{playerX, player.y},
                         {playerX + characterDimensions[player.state][0], player.y - characterDimensions[player.state][1]}};

    int obstacleD[2][2] = {{obstacle.x, obstacle.y},
                           {obstacle.x + obstacle.width, obstacle.y - obstacle.height}};

    // Checks if obstacle is totally left, right, etc. of player (if any is true, they don't overlap)
    if (playerD[0][0] < obstacleD[1][0] && playerD[1][0] > obstacleD[0][0]
        && playerD[0][1] > obstacleD[1][1] && playerD[1][1] < obstacleD[0][1]) {
        return 1;
    } else {
        return 0;
    }
}

int main() {
    time_t t;
    srand((unsigned) time(&t)); // Seeds random number

    BSP_LCD_Init();

    menu();
}